# Stream Generator

Random generator of long arrow runs ("streams") for dance games such as Stepmania and In The Groove. Generates a reasonable stream of a length of your choosing, with adjustable probability for repeat steps ("trills") and quantisation. Outputs note data in [SM file format](https://github.com/stepmania/stepmania/wiki/sm) to standard output (pipe to a file for saving).

example: `./streamgenerator.py -b 16 -t 6` generates a 16-bar stream with medium to high probability of trills.
