#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
//#include <random>
#include <time.h> // visual C++ bullshit

using namespace std;

int numbars, numsteps;
int trills; // probability of trills out of 10

enum { A_NONE = 0, A_LEFT, A_DOWN, A_UP, A_RIGHT, NUMARROWS };

int pickfrom(int arrow1, int arrow2) {
	return ((rand()%2)?arrow1:arrow2);
}

int pickfrom(int arrow1, int arrow2, int arrow3) {
	int c = (rand()%3);
	if(c==0) return arrow1; else if(c==1) return arrow2; else return arrow3;
}

// pick from two arrows with a lower chance of making trills
// this is a lot simpler than working with three arrows due to not having to take into account footswitches etc.
int pickfrom_at(int arrow1, int arrow2, int thisfoot) {
    int a; // arrow to return
    if(thisfoot == arrow1) {
        int r = rand()%10;
        if(r<trills) a = arrow1; else a = arrow2;
	} else if(thisfoot == arrow2) {
        int r = rand()%10;
        if(r<trills) a = arrow2; else a = arrow1;
	}
	else a = pickfrom(arrow1, arrow2);
	return a;
}

// pick from three arrows with a lower chance of making long trills
// otherfoot = arrow last stepped by the other foot; thisfoot = arrow before that one, last stepped by this foot
int pickfrom_at(int arrow1, int arrow2, int arrow3, int thisfoot, int otherfoot) {
    int a; // arrow to return
	if(thisfoot == arrow1) {
        int r = rand()%10;
        if(r<trills) a = arrow1; else a = ((rand()%2)?arrow2:arrow3);
	} else if(thisfoot == arrow2) {
        int r = rand()%10;
        if(r<trills) a = arrow2; else a = ((rand()%2)?arrow3:arrow1);
	} else if(thisfoot == arrow3) {
        int r = rand()%10;
        if(r<trills) a = arrow3; else a = ((rand()%2)?arrow1:arrow2);
	} else a = pickfrom(arrow1, arrow2, arrow3);

	// Footswitch detection and removal
	if(a==A_UP && otherfoot==a) a = A_DOWN;
	if(a==A_DOWN && otherfoot==a) a = A_UP;

	return a;
}

int main(int argc, char** argv)
{
    /*
    time_t now;
    time(&now);
    srand((unsigned int)now);
	*/
	srand(time(NULL));

    numbars = 4;
    numsteps = 16;
	bool startright; // whether or not to start on the right foot

	startright = (rand()%2);

    trills = 3;
    int tt; // temporary variable, initialisation moved from below

    for(int i=1; i<argc; i++) {
        char *a = &argv[i][2];
        if(argv[i][0]=='-') switch(argv[i][1]) {
            case 'b': numbars = atoi(a); break;
            case 'q': numsteps = atoi(a); break;
			case 'l': startright = false; break;
			case 'r': startright = true; break;
			case 't':
                tt = atoi(a);
                if(tt<0 || tt>10) printf("invalid value %d for -t (trills) parameter, valid range is 0-10\n", tt); else trills=tt;
                break;
            default: printf("unknown command line argument\n");
        }

    }

    int numreps = numbars*numsteps;
    int leftfoot = A_NONE, rightfoot = A_NONE;
	int leftprev = A_NONE, rightprev = A_NONE;

    //if((int)(rand()%2))

    vector<int> steps;

    for(int i=0; i<numreps; i++) {
        if(i%2==(!startright)) { // right foot arrow
            if((leftfoot==A_NONE && rightfoot==A_NONE) || leftfoot == A_LEFT) rightfoot = pickfrom_at(A_DOWN, A_UP, A_RIGHT, rightprev, leftfoot); // first step or left foot is on left arrow
            else if(leftfoot == A_DOWN) rightfoot = pickfrom_at(A_UP, A_RIGHT, rightprev);
            else if(leftfoot == A_UP) rightfoot = pickfrom_at(A_DOWN, A_RIGHT, rightprev);
            steps.push_back(rightfoot);
            rightprev=rightfoot;
        } else { // left foot arrow
            if((leftfoot==A_NONE && rightfoot==A_NONE) || rightfoot == A_RIGHT) leftfoot = pickfrom_at(A_LEFT, A_DOWN, A_UP, leftprev, rightfoot);
            else if(rightfoot == A_UP) leftfoot = pickfrom_at(A_LEFT, A_DOWN, leftprev);
            else if(rightfoot == A_DOWN) leftfoot = pickfrom_at(A_LEFT, A_UP, leftprev);
            steps.push_back(leftfoot); // first step
            leftprev=leftfoot;
        }
    }

    printf("Generating a %d bar long %dth stream (%d steps), starting on %s foot.\n", numbars, numsteps, steps.size(), startright?"right":"left");
    printf("Trill probability: %d/10\n", trills);

    // now to print the streams

    for(int i=0; i<numbars; i++) {
        for(int j=0; j<numsteps; j++) {
            printf("%d%d%d%d\n", steps[i*numsteps + j]==A_LEFT, steps[i*numsteps + j]==A_DOWN, steps[i*numsteps + j]==A_UP, steps[i*numsteps + j]==A_RIGHT);
        }
        if(i<numbars-1) printf(",\n");
    }

    return 0;
}
