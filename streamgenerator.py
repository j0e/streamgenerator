#!/usr/bin/env python

import random, sys

random.seed() #seed the random number generator

numbars = 4 # length of stream in bars
numsteps = 16 # quantisation of stream (default 16ths)
trills = 3 # trill probability, can be 0 to 10

#arrow type enum
def enum(**enums):
    return type('Enum', (), enums)

A = enum(NONE=0, LEFT=1, DOWN=2, UP=3, RIGHT=4)


def pickfrom2(arrow1, arrow2):
	r = random.randint(0,1)
	return arrow1 if r else arrow2
	
def pickfrom3(arrow1, arrow2, arrow3):
	r = random.randint(0,2)
	return arrow1 if r==0 else arrow2 if r==1 else arrow3

'''
for i in range(0,5):
	print pickfrom3(1, 2, 3)
'''

# Pick from two arrows with a lower chance of making trills
# this is a lot simpler than working with three arrows due to
# not having to take into account footswitches etc.
def pickfrom2_at(arrow1, arrow2, thisfoot):
	if thisfoot==arrow1:
		r = random.randint(0,9)
		a = arrow1 if r<trills else arrow2
	elif thisfoot==arrow2:
		r = random.randint(0,9)
		a = arrow2 if r<trills else arrow1
	else:
		a = pickfrom2(arrow1, arrow2)
	return a

# pick from three arrows with a lower chance of making long trills
#otherfoot = arrow last stepped by the other foot
# thisfoot = arrow before that one, last stepped by this foot

def pickfrom3_at(arrow1, arrow2, arrow3, thisfoot, otherfoot):
	if thisfoot==arrow1:
		r = random.randint(0,9)
		a = arrow1 if r<trills else arrow2 if random.randint(0,1) else arrow1
	elif thisfoot==arrow2:
		r = random.randint(0,9)
		a = arrow2 if r<trills else arrow3 if random.randint(0,1) else arrow1
	elif thisfoot==arrow3:
		r = random.randint(0,9)
		a = arrow3 if r<trills else arrow1 if random.randint(0,1) else arrow2
	else:
		a = pickfrom3(arrow1, arrow2, arrow3)
	
	# footswitch detection and removal
	if a==A.UP and otherfoot==a:
		a = A.DOWN
	if a==A.DOWN and otherfoot==a:
		a=A.UP
	
	return a

# MAIN #

startright = random.randint(0,1)
trills=3

for i in range(1, len(sys.argv)):
	a = sys.argv[i][2:] if len(sys.argv[i])>2 else "" # hack
	if sys.argv[i][0]=="-":
		if sys.argv[i][1]=="b":
			numbars = int(a)
		elif sys.argv[i][1]=="q":
			numsteps = int(a)
		elif sys.argv[i][1]=="l":
			startright = 0
		elif sys.argv[i][1]=="r":
			startright = 1
		elif sys.argv[i][1]=="t":
			tt = int(a)
			if tt<0 or tt>10:
				print "invalid value %(a)d for -t (trills) parameter, valid range is 0-10" % {"a":tt}
			else:
				trills=tt
		else:
			print "unknown command line argument %(a)s" % {"a":sys.argv[i]}

numreps = numbars*numsteps
leftfoot = A.NONE
rightfoot = A.NONE
leftprev = A.NONE
rightprev = A.NONE

steps = []

for i in range(0,numreps):
	if i%2==startright: # left foot arrow
		if ((leftfoot==A.NONE and rightfoot==A.NONE) or rightfoot==A.RIGHT):
			leftfoot = pickfrom3_at(A.LEFT, A.DOWN, A.UP, leftprev, rightfoot)
		elif rightfoot==A.UP:
			leftfoot = pickfrom2_at(A.LEFT, A.DOWN, leftprev)
		elif rightfoot==A.DOWN:
			leftfoot = pickfrom2_at(A.LEFT, A.UP, leftprev)
		steps.append(leftfoot)
		leftprev=leftfoot
	else:
		if ((leftfoot==A.NONE and rightfoot==A.NONE) or leftfoot==A.LEFT):
			rightfoot = pickfrom3_at(A.DOWN, A.UP, A.RIGHT, rightprev, leftfoot)
		elif leftfoot==A.DOWN:
			rightfoot = pickfrom2_at(A.UP, A.RIGHT, rightprev)
		elif leftfoot==A.UP:
			rightfoot = pickfrom2_at(A.DOWN, A.RIGHT, rightprev)
		steps.append(rightfoot)
		rightprev=rightfoot

print "Generating a %(a)d bar long %(b)dth stream (%(c)d steps), starting on %(d)s foot." % {"a":numbars, "b":numsteps, "c":len(steps), "d":"right" if startright==1 else "left"}
print "Trill probability: %(a)d/10" % {"a":trills}

# now to print the streams

for i in range(0, numbars):
	for j in range(0, numsteps):
		a=1
		print "%(a)d%(b)d%(c)d%(d)d" % {"a":steps[i*numsteps+j]==A.LEFT, "b":steps[i*numsteps+j]==A.DOWN, "c":steps[i*numsteps+j]==A.UP, "d":steps[i*numsteps+j]==A.RIGHT}
	if i<numbars-1:
		print ","